﻿using UnityEngine;
using UniRx;

/// <summary>
/// Obiekt dłoni avatara VR
/// </summary>
public class VRAvatarHand : MonoBehaviour
{
	/// <summary>
	/// Model dłoni avatara
	/// </summary>
	[SerializeField] private VRAvatarHandModel model;
	/// <summary>
	/// Punkt środkowy modelu dłoni
	/// </summary>
	[SerializeField] protected Transform modelRoot;

	public Vector3 Position => transform.position;
	public Quaternion Rotation => transform.rotation;

	/// <summary>
	/// Urządzenie VR trzymane w dłoni odpowiadające tej dłoni avatara
	/// </summary>
	private IHandheld handheld;
	/// <summary>
	/// Przesunięcie względem pozycji zwracanej przez urządzenie VR
	/// </summary>
	private Vector3 positionOffset;
	/// <summary>
	/// Przesunięcie względem pozycji uzależnione od platformy, na której działa aplikacja
	/// </summary>
	private Vector3 platformPositionOffset;
	/// <summary>
	/// Czy dłoń jest widoczna
	/// </summary>
	private bool visible = true;

	public virtual void Initialize(IHandheld handheld)
	{
		this.handheld = handheld;
		handheld.TrackingStream.Subscribe(_ => UpdatePositionAndRotation());
	}
	/// <summary>
	/// Ustawia przesunięcie
	/// </summary>
	/// <param name="offset">Przesunięcie</param>
	public void SetPositionOffset(Vector3 offset)
	{
		positionOffset = offset;
		UpdatePositionAndRotation();
	}
	/// <summary>
	/// Pozwala zmienić warstwę renderowania obiektu
	/// </summary>
	/// <param name="overlay">Czy ma być wyświetlany ponad obiektami świata gry?</param>
	public void SetOverlay(bool overlay)
	{
		model.SetOverlay(overlay);
	}
	/// <summary>
	/// Ustawia model dłoni
	/// </summary>
	/// <param name="handModelPrefab"></param>
	public void SetHandModel(VRAvatarHandModel handModelPrefab)
	{
		Destroy(model.gameObject);
		model = Instantiate(handModelPrefab, modelRoot);
		model.transform.localPosition = Vector3.zero;
		model.transform.localRotation = Quaternion.identity;

		if (visible) model.Show();
		else model.Hide();
	}
	/// <summary>
	/// Pokazuje model dłoni
	/// </summary>
	public void Show()
	{
		model.Show();
		visible = true;
	}
	/// <summary>
	/// Chowa model dłoni
	/// </summary>
	public void Hide()
	{
		model.Hide();
		visible = false;
	}
	/// <summary>
	/// Aktualizuje pozycję i rotację dłoni zgodnie z informacjami przesyłanymi przez urządzenie VR
	/// </summary>
	private void UpdatePositionAndRotation()
	{
		transform.SetPositionAndRotation(handheld.Position + positionOffset, handheld.Rotation);
	}

	/// <summary>
	/// Przekazuje żądanie sugnału drgania
	/// </summary>
	/// <param name="signalType">Typ sygnału</param>
	public void RequestHapticSignal(HapticSignal signalType)
	{
		handheld.SendHapticSignal(signalType);
	}
}
