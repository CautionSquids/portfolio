﻿using System;
using UniRx;

public interface IHandheld : IVRInputDevice
{
	IObservable<float> TriggerStream { get; }
	IObservable<float> TriggerPressedTimedStream { get; }
	IObservable<bool> TriggerPressedStream { get; }
	IObservable<Unit> TriggerDownStream { get; }
	IObservable<Unit> TriggerHeldStream { get; }
	IObservable<Unit> TriggerUpStream { get; }

	IObservable<float> GripValueSetStream { get; }
	IObservable<float> GripPressedTimedStream { get; }
	IObservable<bool> GripPressedStream { get; }
	IObservable<Unit> GripDownStream { get; }
	IObservable<Unit> GripHeldStream { get; }
	IObservable<Unit> GripUpStream { get; }

	IObservable<bool> PrimaryButtonValueSetStream { get; }
	IObservable<float> PrimaryButtonPressedTimedStream { get; }
	IObservable<Unit> PrimaryButtonDownStream { get; }
	IObservable<Unit> PrimaryButtonHeldStream { get; }
	IObservable<Unit> PrimaryButtonUpStream { get; }

	IObservable<bool> SecondaryButtonValueSetStream { get; }
	IObservable<float> SecondaryButtonPressedTimedStream { get; }
	IObservable<Unit> SecondaryButtonDownStream { get; }
	IObservable<Unit> SecondaryButtonHeldStream { get; }
	IObservable<Unit> SecondaryButtonUpStream { get; }

	void SendHapticSignal(HapticSignal signalType);
}
