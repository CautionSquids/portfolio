﻿using System;
using UniRx;

public interface IVRInputProvider
{
	IObservable<Unit> AnyTriggerDownStream { get; }
	IObservable<Unit> AnyTriggerUpStream { get; }
	IObservable<Unit> AnyTriggerHeldStream { get; }

	IObservable<Unit> AnyGripDownStream { get; }
	IObservable<Unit> AnyGripUpStream { get; }
	IObservable<Unit> AnyGripHeldStream { get; }

	IObservable<Unit> AnyPrimaryButtonDownStream { get; }
	IObservable<Unit> AnyPrimaryButtonHeldStream { get; }
	IObservable<Unit> AnyPrimaryButtonUpStream { get; }

	IObservable<Unit> AnySecondaryButtonDownStream { get; }
	IObservable<Unit> AnySecondaryButtonHeldStream { get; }
	IObservable<Unit> AnySecondaryButtonUpStream { get; }

	IVRInputDevice HeadsetNode { get; }
	IHandheld LeftHandNode { get; }
	IHandheld RightHandNode { get; }
}
