﻿using UnityEngine;
using System;
using UniRx;

/// <summary>
/// Obszar gry
/// </summary>
public class VRPlayableArea : MonoBehaviour
{
	/// <summary>
	/// Domyślny obszar przeznaczony do gry
	/// </summary>
	[SerializeField] private Area defaultPlayableArea;
	/// <summary>
	/// Szerokość obszaru ostrzegawczego
	/// </summary>
	[SerializeField] private float warningAreaWidth;
	/// <summary>
	/// Wizualizacja obszaru gry
	/// </summary>
	[SerializeField] private Renderer tube;

	/// <summary>
	/// Stream informujący o wejściu w obszar ostrzegawczy
	/// </summary>
	public IObservable<Unit> PlayerEnterWarningAreaStream => warningAreaSubject.Pairwise((prev, next) => !prev && next).Where(x => x).AsUnitObservable();
	/// <summary>
	/// Stream informujący o opuszczeniu obszaru ostrzegawczego
	/// </summary>
	public IObservable<Unit> PlayerExitWarningAreaStream => warningAreaSubject.Pairwise((prev, next) => prev && !next).Where(x => x).AsUnitObservable();
	/// <summary>
	/// Stream informujący o wejściu w obszar niedozwolony
	/// </summary>
	public IObservable<Unit> PlayerEnterForbiddenAreaStream => forbiddenAreaSubject.Pairwise((prev, next) => !prev && next).Where(x => x).AsUnitObservable();
	/// <summary>
	/// Stream informujący o opuszczeniu obszaru niedozwolonego
	/// </summary>
	public IObservable<Unit> PlayerExitForbiddenAreaStream => forbiddenAreaSubject.Pairwise((prev, next) => prev && !next).Where(x => x).AsUnitObservable();
	/// <summary>
	/// Stream informujący o odległości od dozwolonego obszaru gry
	/// </summary>
	public IObservable<float> PlayerDistanceFromPlayableAreaStream => distanceFromPlayableAreaSubject.AsObservable();

	private BehaviorSubject<bool> warningAreaSubject = new BehaviorSubject<bool>(false);
	private BehaviorSubject<bool> forbiddenAreaSubject = new BehaviorSubject<bool>(false);
	private Subject<float> distanceFromPlayableAreaSubject = new Subject<float>();
	/// <summary>
	/// Dozwolony obszar gry
	/// </summary>
	private Area playableArea;
	/// <summary>
	/// Dozwolony obszar gry powiększony o szerokość obszaru zagrożenia
	/// </summary>
	private Area warningArea;
	/// <summary>
	/// Przesunięcie pozycji
	/// </summary>
	private Vector3 positionOffset;
	/// <summary>
	/// Gracz
	/// </summary>
	private IPlayer player;
	private Vector3 worldPosition => transform.position + positionOffset;

	public void Initialize(IPlayer player)
	{
		this.player = player;

		playableArea = defaultPlayableArea;
		warningArea = new Area(playableArea);
		warningArea.Extend(warningAreaWidth);

		warningAreaSubject.OnNext(false);
		forbiddenAreaSubject.OnNext(false);

		Hide();
	}
	/// <summary>
	/// Centruje obszar gry w pozycji
	/// </summary>
	/// <param name="worldPosition">Środek obszaru gry</param>
	public void CenterOn(Vector3 worldPosition)
	{
		SetPositionOffset(worldPosition - transform.position);
	}
	/// <summary>
	/// Ustawia przesunięcie pozycji
	/// </summary>
	/// <param name="offset">Przesunięcie</param>
	public void SetPositionOffset(Vector3 offset)
	{
		positionOffset = offset;
	}
	/// <summary>
	/// Sprawdza pozycję gracza
	/// </summary>
	public void CheckPlayerPosition()
	{
		warningAreaSubject.OnNext(!playableArea.OverlapPoint(worldPosition, player.HeadPosition));
		forbiddenAreaSubject.OnNext(!warningArea.OverlapPoint(worldPosition, player.HeadPosition));
		distanceFromPlayableAreaSubject.OnNext(playableArea.DistanceToPoint(worldPosition, player.HeadPosition) / warningAreaWidth);
	}
	/// <summary>
	/// Pokazuje obszar gry
	/// </summary>
	public void Show()
	{
		tube.gameObject.SetActive(true);
	}
	/// <summary>
	/// Chowa obszar gry
	/// </summary>
	public void Hide()
	{
		tube.gameObject.SetActive(false);
	}
	/// <summary>
	/// Resetuję obszar gry
	/// </summary>
	public void ResetArea()
	{
		playableArea = defaultPlayableArea;
		positionOffset = Vector3.zero;
	}
	private void OnDrawGizmosSelected()
	{
		playableArea.DrawShapeGizmo(transform.position + positionOffset, Color.yellow);
		warningArea.DrawShapeGizmo(transform.position + positionOffset, Color.red);
	}
}
