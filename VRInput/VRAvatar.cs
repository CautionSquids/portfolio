﻿using System;
using UniRx;
using UnityEngine;

/// <summary>
/// Reprezentacja człowieka ubranego w sprzęt VR
/// </summary>
public class VRAvatar : VRAvatar<VRAvatarHand> { }
/// <summary>
/// Reprezentacja człowieka ubranego w sprzęt VR
/// </summary>
/// <typeparam name="THand">Typ dłoni</typeparam>
public class VRAvatar<THand> : MonoBehaviour where THand : VRAvatarHand
{
	/// <summary>
	/// Głowa avatara
	/// </summary>
	[SerializeField] protected VRAvatarHead head;
	/// <summary>
	/// Lewa dłoń avatara
	/// </summary>
	[SerializeField] protected THand leftHand;
	/// <summary>
	/// Prawa dłoń avatara
	/// </summary>
	[SerializeField] protected THand rightHand;
	/// <summary>
	/// Domyślny model dłoni avatara
	/// </summary>
	[SerializeField] protected VRAvatarHandModel defaultHandModel;

	/// <summary>
	/// Stream informujący o zresetowaniu pozycji avatara
	/// </summary>
	public IObservable<Unit> PlayerPositionResetStream => playerPositionResetSubject.AsObservable();

	protected IVRInputProvider inputProvider;

	private Subject<Unit> playerPositionResetSubject = new Subject<Unit>();
	/// <summary>
	/// Wartość dodawana do wzrostu gracza
	/// </summary>
	private float heightAdjustment;

	public virtual void Initialize(IVRInputProvider inputProvider)
	{
		this.inputProvider = inputProvider;

		leftHand.Initialize(inputProvider.LeftHandNode);
		rightHand.Initialize(inputProvider.RightHandNode);
		head.Initialize(inputProvider.HeadsetNode);

		SetDefaultHandModels();
	}
	/// <summary>
	/// Resetuje pozycję avatara
	/// </summary>
	public virtual void ResetPosition()
	{
		Vector3 offset = GetPlayerOffset();
		offset.y = heightAdjustment;
		head.SetPositionOffset(offset);
		leftHand.SetPositionOffset(offset);
		rightHand.SetPositionOffset(offset);

		playerPositionResetSubject.OnNext(Unit.Default);
	}
	/// <summary>
	/// Ustawia wzrost avatara
	/// </summary>
	/// <param name="heightInMeters">Wzrost (w metrach)</param>
	public void SetPlayerHeight(float heightInMeters)
	{
		heightAdjustment = heightInMeters;
	}
	/// <summary>
	/// Zwraca przesunięcie gracza względem pozycji zwracanej przez urządzenie VR
	/// </summary>
	/// <returns></returns>
	protected virtual Vector3 GetPlayerOffset()
	{
		return -inputProvider.HeadsetNode.Position;
	}

	/// <summary>
	/// Wysyła żądanie wysłania sygnału drgania
	/// </summary>
	/// <param name="handSide">Urządzenie, do którego ma zostać przesłane żądanie</param>
	/// <param name="signalType">Typ sygnału drgań</param>
	public void RequestHapticSignal(HandSide handSide, HapticSignal signalType)
	{
		if (TryGetHand(handSide, out THand hand))
		{
			hand.RequestHapticSignal(signalType);
		}
	}
	/// <summary>
	/// Przywraca domyślne modele dłoni avatara
	/// </summary>
	public void SetDefaultHandModels()
	{
		SetHandModels(defaultHandModel);
	}
	/// <summary>
	/// Pokazuje modele dłoni
	/// </summary>
	public void ShowHands()
	{
		leftHand.Show();
		rightHand.Show();
	}
	/// <summary>
	/// Chowa modele dłoni
	/// </summary>
	public void HideHands()
	{
		leftHand.Hide();
		rightHand.Hide();
	}
	/// <summary>
	/// Ustawia modele dłoni
	/// </summary>
	/// <param name="handModelPrefab">Model dłoni</param>
	public void SetHandModels(VRAvatarHandModel handModelPrefab)
	{
		leftHand.SetHandModel(handModelPrefab);
		rightHand.SetHandModel(handModelPrefab);
	}
	/// <summary>
	/// Zwraca domyślny model dłoni
	/// </summary>
	/// <returns></returns>
	public VRAvatarHandModel GetDefaultHandModel()
	{
		return defaultHandModel;
	}
	/// <summary>
	/// Zwraca obiekt dłoni, w przypadku, kiedy obiekt ten istnieje
	/// </summary>
	/// <param name="handSide">Dłoń, dla której obiekt ma zostać zwrócony</param>
	/// <param name="hand">Zwracany obiekt dłoni</param>
	/// <returns></returns>
	private bool TryGetHand(HandSide handSide, out THand hand)
	{
		switch (handSide)
		{
			case HandSide.Left: hand = leftHand; return true;
			case HandSide.Right: hand = rightHand; return true;
			case HandSide.None: default: hand = default; return false;
		}
	}
}
