﻿using System;
using UniRx;
using UnityEngine;
using UnityEngine.XR;

/// <summary>
/// Bazowa klasa wszystkich urządzeń VR
/// </summary>
public abstract class VRInputDevice : IVRInputDevice
{
	/// <summary>
	/// Stream informujący o połączeniu tego urządzenia
	/// </summary>
	public IObservable<Unit> ConnectedStream => connectedSubject.AsObservable();
	/// <summary>
	/// Stream informujący o rozłączeniu tego urządzenia
	/// </summary>
	public IObservable<Unit> DisconnectedStream => disconnectedSubject.AsObservable();
	/// <summary>
	/// Stream informujący o wznowieniu śledzenia tego urządzenia
	/// </summary>
	public IObservable<Unit> TrackingAcquiredStream => trackedSubject.Pairwise((prev, next) => !prev && next).Where(x => x).AsUnitObservable();
	/// <summary>
	/// Stream informujący o śledzeniu tego urządzenia
	/// </summary>
	public IObservable<Unit> TrackingStream => trackedSubject.Pairwise((prev, next) => prev && next).Where(x => x).AsUnitObservable();
	/// <summary>
	/// Stream informujący o utracie śledzenia tego urządzenia
	/// </summary>
	public IObservable<Unit> TrackingLostStream => trackedSubject.Pairwise((prev, next) => prev && !next).Where(x => x).AsUnitObservable();

	public Vector3 Position { get; private set; }
	public Quaternion Rotation { get; private set; }
	public bool IsValid => device.isValid;

	/// <summary>
	/// Dane o tym urządzeniu
	/// </summary>
	protected InputDevice device;

	private Subject<bool> trackedSubject = new Subject<bool>();
	private Subject<Unit> connectedSubject = new Subject<Unit>();
	private Subject<Unit> disconnectedSubject = new Subject<Unit>();

	public VRInputDevice() { }
	/// <summary>
	/// Akcja wywoływana po połączeniu urządzenia
	/// </summary>
	/// <param name="device">Dane urządzenia</param>
	public virtual void OnConnect(InputDevice device)
	{
		this.device = device;
		connectedSubject.OnNext(Unit.Default);
	}
	/// <summary>
	/// Akcja wywoływana po rozłączeniu urządzenia
	/// </summary>
	public virtual void OnDisconnect()
	{
		disconnectedSubject.OnNext(Unit.Default);
	}
	/// <summary>
	/// Akcja wywoływana, gdy urządzenie jest podłączone
	/// </summary>
	public virtual void UpdateConnected()
	{
		UpdateFeaturesValues();
	}

	/// <summary>
	/// Aktualizuje dane o urządzeniu
	/// </summary>
	private void UpdateFeaturesValues()
	{
		if (device.TryGetFeatureValue(CommonUsages.isTracked, out bool tracked)) trackedSubject.OnNext(tracked);
		if (device.TryGetFeatureValue(CommonUsages.devicePosition, out Vector3 position)) Position = position;
		if (device.TryGetFeatureValue(CommonUsages.deviceRotation, out Quaternion rotation)) Rotation = rotation;
	}
}
