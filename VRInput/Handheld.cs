﻿using System;
using UniRx;
using UnityEngine.XR;

/// <summary>
/// Urządzenie VR trzymane w dłoni
/// </summary>
public class Handheld : VRInputDevice, IHandheld
{
	/// <summary>
	/// Stream informujący o zmianie wartości przycisku Trigger urządzenia
	/// </summary>
	public IObservable<float> TriggerStream => triggerSubject.AsObservable();
	/// <summary>
	/// Stream informujący o czasie przytrzymania przycisku Trigger urządzenia
	/// </summary>
	public IObservable<float> TriggerPressedTimedStream => Observable.EveryUpdate().SkipUntil(TriggerDownStream).TakeUntil(TriggerUpStream).Select(_ => UnityEngine.Time.deltaTime).Scan((x, y) => x + y).Repeat();
	/// <summary>
	/// Stream informujący o stanie wciśnięcia przycisku Trigger urządzenia
	/// </summary>
	public IObservable<bool> TriggerPressedStream => TriggerStream.Select(value => value >= triggerPressThreshold);
	/// <summary>
	/// Stream informujący o wciśnięciu przycisku Trigger urządzenia
	/// </summary>
	public IObservable<Unit> TriggerDownStream => TriggerPressedStream.Pairwise((prev, next) => !prev && next).Where(x => x).AsUnitObservable();
	/// <summary>
	/// Stream informujący o trzymaniu przycisku Trigger urządzenia
	/// </summary>
	public IObservable<Unit> TriggerHeldStream => TriggerPressedStream.Pairwise((prev, next) => prev && next).Where(x => x).AsUnitObservable();
	/// <summary>
	/// Stream informujący o zwolnieniu przycisku Trigger urządzenia
	/// </summary>
	public IObservable<Unit> TriggerUpStream => TriggerPressedStream.Pairwise((prev, next) => prev && !next).Where(x => x).AsUnitObservable();

	/// <summary>
	/// Stream informujący o zmianie wartości przycisku Grip urządzenia
	/// </summary>
	public IObservable<float> GripValueSetStream => gripSubject.AsObservable();
	/// <summary>
	/// Stream informujący o czasie przytrzymania przycisku Grip urządzenia
	/// </summary>
	public IObservable<float> GripPressedTimedStream => Observable.EveryUpdate().SkipUntil(GripDownStream).TakeUntil(GripUpStream).Select(_ => UnityEngine.Time.deltaTime).Scan((x, y) => x + y).Repeat();
	/// <summary>
	/// Stream informujący o stanie wciśnięcia przycisku Grip urządzenia
	/// </summary>
	public IObservable<bool> GripPressedStream => GripValueSetStream.Select(value => value >= gripPressThreshold);
	/// <summary>
	/// Stream informujący o wciśnięciu przycisku Grip urządzenia
	/// </summary>
	public IObservable<Unit> GripDownStream => GripPressedStream.Pairwise((prev, next) => !prev && next).Where(x => x).AsUnitObservable();
	/// <summary>
	/// Stream informujący o trzymaniu przycisku Grip urządzenia
	/// </summary>
	public IObservable<Unit> GripHeldStream => GripPressedStream.Pairwise((prev, next) => prev && next).Where(x => x).AsUnitObservable();
	/// <summary>
	/// Stream informujący o zwolnieniu przycisku Grip urządzenia
	/// </summary>
	public IObservable<Unit> GripUpStream => GripPressedStream.Pairwise((prev, next) => prev && !next).Where(x => x).AsUnitObservable();

	/// <summary>
	/// Stream informujący o stanie wciśnięcia przycisku Primary urządzenia
	/// </summary>
	public IObservable<bool> PrimaryButtonValueSetStream => primaryButtonSubject.AsObservable();
	/// <summary>
	/// Stream informujący o czasie trzymania przycisku Primary urządzenia
	/// </summary>
	public IObservable<float> PrimaryButtonPressedTimedStream => Observable.EveryUpdate().SkipUntil(PrimaryButtonDownStream).TakeUntil(PrimaryButtonUpStream).Select(_ => UnityEngine.Time.deltaTime).Scan((x, y) => x + y).Repeat();
	/// <summary>
	/// Stream informujący o wciśnięciu przycisku Primary urządzenia
	/// </summary>
	public IObservable<Unit> PrimaryButtonDownStream => PrimaryButtonValueSetStream.Pairwise((prev, next) => !prev && next).Where(x => x).AsUnitObservable();
	/// <summary>
	/// Stream informujący o trzymaniu przycisku Primary urządzenia
	/// </summary>
	public IObservable<Unit> PrimaryButtonHeldStream => PrimaryButtonValueSetStream.Pairwise((prev, next) => prev && next).Where(x => x).AsUnitObservable();
	/// <summary>
	/// Stream informujący o zwolnieniu przycisku Primary urządzenia
	/// </summary>
	public IObservable<Unit> PrimaryButtonUpStream => PrimaryButtonValueSetStream.Pairwise((prev, next) => prev && !next).Where(x => x).AsUnitObservable();

	/// <summary>
	/// Stream informujący o stanie wciśnięcia przycisku Secondary urządzenia
	/// </summary>
	public IObservable<bool> SecondaryButtonValueSetStream => primaryButtonSubject.AsObservable();
	/// <summary>
	/// Stream informujący o czasie trzymania przycisku Secondary urządzenia
	/// </summary>
	public IObservable<float> SecondaryButtonPressedTimedStream => Observable.EveryUpdate().SkipUntil(SecondaryButtonDownStream).TakeUntil(SecondaryButtonUpStream).Select(_ => UnityEngine.Time.deltaTime).Scan((x, y) => x + y).Repeat();
	/// <summary>
	/// Stream informujący o wciśnięciu przycisku Secondary urządzenia
	/// </summary>
	public IObservable<Unit> SecondaryButtonDownStream => SecondaryButtonValueSetStream.Pairwise((prev, next) => !prev && next).Where(x => x).AsUnitObservable();
	/// <summary>
	/// Stream informujący o trzymaniu przycisku Secondary urządzenia
	/// </summary>
	public IObservable<Unit> SecondaryButtonHeldStream => SecondaryButtonValueSetStream.Pairwise((prev, next) => prev && next).Where(x => x).AsUnitObservable();
	/// <summary>
	/// Stream informujący o zwolnieniu przycisku Secondary urządzenia
	/// </summary>
	public IObservable<Unit> SecondaryButtonUpStream => SecondaryButtonValueSetStream.Pairwise((prev, next) => prev && !next).Where(x => x).AsUnitObservable();

	private BehaviorSubject<float> triggerSubject = new BehaviorSubject<float>(0f);
	private BehaviorSubject<float> gripSubject = new BehaviorSubject<float>(0f);
	private Subject<bool> primaryButtonSubject = new Subject<bool>();
	private Subject<bool> secondaryButtonSubject = new Subject<bool>();

	/// <summary>
	/// Wartość przycisku Trigger, powyżej której przycisk uznawany jest za wciśnięty
	/// </summary>
	private const float triggerPressThreshold = .5f;
	/// <summary>
	/// Wartość przycisku Grip, powyżej której przycisk uznawany jest za wciśnięty
	/// </summary>
	private const float gripPressThreshold = .5f;

	/// <summary>
	/// Tablica bajtów definiująca sygnał drgania typu Impulse
	/// </summary>
	private byte[] impulse = new byte[] { 10 };
	/// <summary>
	/// Tablica bajtów definiująca sygnał drgania typu Success
	/// </summary>
	private byte[] success = new byte[] { 100, 0, 100 };
	/// <summary>
	/// Tablica bajtów definiująca sygnał drgania typu Failure
	/// </summary>
	private byte[] failure = new byte[] { 100, 90, 80, 70, 60, 50, 40, 30, 20, 10 };

	/// <summary>
	/// Czas trwania sygnału drgania typu Impulse
	/// </summary>
	private const float impulseDuration = .05f;
	/// <summary>
	/// Czas trwania sygnału drgania typu Success
	/// </summary>
	private const float successDuration = .5f;
	/// <summary>
	/// Czas trwania sygnału drgania typu Failure
	/// </summary>
	private const float failureDuration = .5f;

	public Handheld() : base() { }
	/// <summary>
	/// Akcja wywoływana po połączeniu urządzenia
	/// </summary>
	/// <param name="device">Dane połączonego urządzenia</param>
	public override void OnConnect(InputDevice device)
	{
		base.OnConnect(device);
		ConvertHapticBuffers();
	}
	/// <summary>
	/// Konwertuje sygnały drgań w przypadku różnej częstotliwości silnika drgającego urządzenia
	/// </summary>
	private void ConvertHapticBuffers()
	{
		if (device.TryGetHapticCapabilities(out HapticCapabilities haptic))
		{
			ConvertHapticBuffer(ref impulse, haptic.bufferFrequencyHz, impulseDuration);
			ConvertHapticBuffer(ref success, haptic.bufferFrequencyHz, successDuration);
			ConvertHapticBuffer(ref failure, haptic.bufferFrequencyHz, failureDuration);

			UnityEngine.Debug.Log($"Converted haptic buffers to {haptic.bufferFrequencyHz} Hz");
		}
	}
	/// <summary>
	/// Konwertuje sygnały drgań w przypadku różnej częstotliwości silnika drgającego urządzenia
	/// </summary>
	/// <param name="buffer">Przekonwertowana tablica bajtów</param>
	/// <param name="bufferReadFrequency">Częstotliwość odczytywania bufferu definiującego sygnał</param>
	/// <param name="bufferDuration">Czas trwania sygnału</param>
	private void ConvertHapticBuffer(ref byte[] buffer, uint bufferReadFrequency, float bufferDuration)
	{
		int copies = UnityEngine.Mathf.CeilToInt(bufferDuration / buffer.Length * bufferReadFrequency);
		byte[] newBuffer = new byte[buffer.Length * copies];
		for (int b = 0; b < buffer.Length; b++)
			for (int c = 0; c < copies; c++)
			{
				newBuffer[b * copies + c] = buffer[b];
			}
		buffer = newBuffer;
	}
	/// <summary>
	/// Wysyła sygnał drgający
	/// </summary>
	/// <param name="signal">Typ sygnału</param>
	public void SendHapticSignal(HapticSignal signal)
	{
		device.SendHapticBuffer(0, HapticBufferFromSignal(signal));
	}
	/// <summary>
	/// Zwraca tablicę bajtów definiującą sygnał
	/// </summary>
	/// <param name="signal">Typ sygnału</param>
	/// <returns></returns>
	private byte[] HapticBufferFromSignal(HapticSignal signal)
	{
		switch (signal)
		{
			case HapticSignal.Impulse: return impulse;
			case HapticSignal.Success: return success;
			case HapticSignal.Failure: return failure;
			default: return new byte[] { };
		}
	}

	/// <summary>
	/// Metoda wykonywana, gdy urządzenie jest podłączone
	/// </summary>
	public override void UpdateConnected()
	{
		base.UpdateConnected();
		UpdateFeaturesValues();
	}

	/// <summary>
	/// Aktualizuje informacje przesyłane przez urządzenie
	/// </summary>
	private void UpdateFeaturesValues()
	{
		if (device.TryGetFeatureValue(CommonUsages.trigger, out float trigger)) triggerSubject.OnNext(trigger);
		if (device.TryGetFeatureValue(CommonUsages.grip, out float grip)) gripSubject.OnNext(grip);
		if (device.TryGetFeatureValue(CommonUsages.primaryButton, out bool primaryButton)) primaryButtonSubject.OnNext(primaryButton);
		if (device.TryGetFeatureValue(CommonUsages.secondaryButton, out bool secondaryButton)) secondaryButtonSubject.OnNext(secondaryButton);
	}
}

/// <summary>
/// Typ sygnału drgań urządzenia VR
/// </summary>
public enum HapticSignal
{
	Impulse,
	Success,
	Failure
}